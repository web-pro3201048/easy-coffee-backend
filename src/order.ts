import { log } from "console";
import { AppDataSource } from "./data-source"
import { User } from "./entity/User";
import { Product } from "./entity/Product";
import { OrderItem } from "./entity/OrderItem";
import { Order } from "./entity/Order";

const orderDto = {
    orderItems: [
        { productId: 1, qty: 1 },
        { productId: 2, qty: 2 },
        { productId: 4, qty: 1 },
    ],
    userId: 2,
};

AppDataSource.initialize().then(async () => {
    const usersrepository = AppDataSource.getRepository(User)
    const productRepository = AppDataSource.getRepository(Product)
    const orderItemsRepository = AppDataSource.getRepository(OrderItem)
    const orderRepository = AppDataSource.getRepository(Order)
    const user = await usersrepository.findOneBy({ id: orderDto.userId });

    const order = new Order()
    order.user = user;
    order.total = 0;
    order.qty = 0;
    order.orderItems = [];
    for (const oi of orderDto.orderItems) {
        const orderItem = new OrderItem()
        orderItem.product = await productRepository.findOneBy({ id: oi.productId, })
        orderItem.name = orderItem.product.name
        orderItem.price = orderItem.product.price
        orderItem.qty = oi.qty
        orderItem.total = orderItem.price * orderItem.qty
        await orderItemsRepository.save(orderItem);
        order.orderItems.push(orderItem);
        order.total += orderItem.total;
        order.qty += orderItem.qty;
    }
    await orderRepository.save(order);

    const orders = await orderRepository.find({ relations: { orderItems: true, user: true }, })
    console.log(JSON.stringify(orders, null, 2))

}).catch(error => console.log(error))
